package com.ayakovlev.fc;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.ayakovlev.fc.bean.Group;
import com.ayakovlev.fc.bean.Team;
import com.ayakovlev.fc.service.GroupService;
import com.ayakovlev.fc.service.TeamService;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		System.out.println("hello world, I have just started up");
		initDatabase();
	}

	private GroupService groupService;

	@Autowired
	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

	private TeamService teamService;

	@Autowired
	public void setTeamService(TeamService teamService) {
		this.teamService = teamService;
	}

	private void initDatabase() {
// Add Groups
		groupService.add(new Group("A"));
		groupService.add(new Group("B"));
		groupService.add(new Group("C"));
		groupService.add(new Group("D"));
		groupService.add(new Group("E"));
		groupService.add(new Group("F"));
		groupService.add(new Group("G"));
		groupService.add(new Group("H"));

// Add Teams
		teamService.add(new Team("Египет"));
		teamService.add(new Team("Марокко"));
		teamService.add(new Team("Нигерия"));
		teamService.add(new Team("Сенегал"));
		teamService.add(new Team("Тунис"));
		teamService.add(new Team("Австралия"));
		teamService.add(new Team("Иран"));
		teamService.add(new Team("Саудовская Аравия"));
		teamService.add(new Team("Южная Корея"));
		teamService.add(new Team("Япония"));
		teamService.add(new Team("Англия"));
		teamService.add(new Team("Бельгия"));
		teamService.add(new Team("Германия"));
		teamService.add(new Team("Дания"));
		teamService.add(new Team("Исландия"));
		teamService.add(new Team("Испания"));
		teamService.add(new Team("Польша"));
		teamService.add(new Team("Португалия"));
		teamService.add(new Team("Россия"));
		teamService.add(new Team("Сербия"));
		teamService.add(new Team("Франция"));
		teamService.add(new Team("Хорватия"));
		teamService.add(new Team("Швейцария"));
		teamService.add(new Team("Швеция"));
		teamService.add(new Team("Коста-Рика"));
		teamService.add(new Team("Мексика"));
		teamService.add(new Team("Панама"));
		teamService.add(new Team("Аргентина"));
		teamService.add(new Team("Бразилия"));
		teamService.add(new Team("Колумбия"));
		teamService.add(new Team("Уругвай"));
		teamService.add(new Team("Перу"));
		
// Get Groups
		Set<Group> groups = groupService.listGroups();
		groups.forEach((Group group) -> {
			System.out.println(group);
//			log.info(group);
		});

// Get Teams
		List<Team> teams = teamService.listTeams();
		teams.forEach((Team team) -> {
			System.out.println(team);
//			log.info(team);
		});

	}

}