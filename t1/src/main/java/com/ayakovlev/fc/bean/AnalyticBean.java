package com.ayakovlev.fc.bean;

public class AnalyticBean {
	private Team team;
	private long wins;
	private long draw;
	private long defeat;
	private long clogged;
	private long missing;
	private long scores;
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public long getWins() {
		return wins;
	}
	public void setWins(long wins) {
		this.wins = wins;
	}
	public long getDraw() {
		return draw;
	}
	public void setDraw(long draw) {
		this.draw = draw;
	}
	public long getDefeat() {
		return defeat;
	}
	public void setDefeat(long defeat) {
		this.defeat = defeat;
	}
	public long getClogged() {
		return clogged;
	}
	public void setClogged(long clogged) {
		this.clogged = clogged;
	}
	public long getMissing() {
		return missing;
	}
	public void setMissing(long missing) {
		this.missing = missing;
	}
	public long getScores() {
		return scores;
	}
	public void setScores(long scores) {
		this.scores = scores;
	}

}
