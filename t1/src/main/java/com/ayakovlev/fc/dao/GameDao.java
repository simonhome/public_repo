package com.ayakovlev.fc.dao;

import java.util.List;

import com.ayakovlev.fc.bean.Game;

public interface GameDao {

	Game get(Long teamId1, Long teamId2);

	void save(Game game);

	List<Game> getGames();

}
