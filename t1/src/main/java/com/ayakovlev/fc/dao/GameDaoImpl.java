package com.ayakovlev.fc.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ayakovlev.fc.bean.Game;

@Repository
public class GameDaoImpl implements GameDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Game get(Long teamId1, Long teamId2) {
		String hql = "from Game gm where gm.teamId1 = :teamId1 and gm.teamId2 = :teamId2";
		List<Game> list = sessionFactory.getCurrentSession().createQuery(hql)
				.setParameter("teamId1", teamId1)
				.setParameter("teamId2", teamId2)
		.list();
		if(list != null && list.size() > 0) {
			return list.get(0); 
		}
		return (Game)sessionFactory.getCurrentSession().createQuery(hql)
				.setParameter("teamId1", teamId2)
				.setParameter("teamId2", teamId1)
				.setMaxResults(1).getResultList().stream().findFirst().orElse(null);
	}

	@Override
	public void save(Game game) {
		sessionFactory.getCurrentSession().persist(game);
	}

	@Override
	public List<Game> getGames() {
		@SuppressWarnings("unchecked")
		TypedQuery<Game> query = sessionFactory.getCurrentSession().createQuery("from Game order by groupName");
		return query.getResultList();
	}

}
