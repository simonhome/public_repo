package com.ayakovlev.fc.dao;

import java.util.List;

import com.ayakovlev.fc.bean.Team;

public interface TeamDao {
	void add (Team team);
	List<Team> listTeams();
	List<Team> undistributedTeams();
	void save(Team team);
	Team get(Long teamId);
}
