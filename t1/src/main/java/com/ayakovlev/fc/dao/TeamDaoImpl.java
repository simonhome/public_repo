package com.ayakovlev.fc.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ayakovlev.fc.bean.Team;

@Repository
public class TeamDaoImpl implements TeamDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void add(Team team) {
		sessionFactory.getCurrentSession().save(team);
	}

	@Override
	public List<Team> listTeams() {
		@SuppressWarnings("unchecked")
		TypedQuery<Team> query = sessionFactory.getCurrentSession().createQuery("from Team");
		return query.getResultList();
	}

	@Override
	public List<Team> undistributedTeams() {
		@SuppressWarnings("unchecked")
		TypedQuery<Team> query = sessionFactory.getCurrentSession().createQuery("from Team where group is null");
		return query.getResultList();
	}

	@Override
	public void save(Team team) {
		sessionFactory.getCurrentSession().persist(team);
	}

	@Override
	public Team get(Long teamId) {
		try {
			return (Team) sessionFactory.getCurrentSession().get(Team.class, teamId);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
