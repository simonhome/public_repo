package com.ayakovlev.fc.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ayakovlev.fc.bean.Group;
import com.ayakovlev.fc.dao.GroupDao;

@Service()
public class GroupServiceImp implements GroupService{

	@Autowired
	private GroupDao groupDao;
	
	@Transactional
	@Override
	public void add(Group group) {
		groupDao.add(group);
	}

	@Transactional(readOnly = true)
	@Override
	public Set<Group> listGroups() {
		Set<Group> list = groupDao.listGroups();
//		list.forEach((Group g) -> {
//			Hibernate.initialize(g.getTeams());
//		});
		return list;
	}

}
