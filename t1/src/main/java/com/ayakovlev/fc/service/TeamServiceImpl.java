package com.ayakovlev.fc.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ayakovlev.fc.bean.Group;
import com.ayakovlev.fc.bean.Team;
import com.ayakovlev.fc.dao.GroupDao;
import com.ayakovlev.fc.dao.TeamDao;

@Service
public class TeamServiceImpl implements TeamService {

	@Autowired
	private TeamDao teamDao;
	
	@Autowired
	private GroupDao groupDao;
	
	@Transactional
	@Override
	public void add(Team team) {
		teamDao.add(team);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Team> listTeams() {
		return teamDao.listTeams();
	}

	@Transactional(readOnly = true)
	@Override
	public List<Team> getUndistributedTeams() {
		return teamDao.undistributedTeams();
	}

	@Transactional
	@Override
	public void distributeTeamsByGroups() {
		List<Team> teamsToDistr = getUndistributedTeams();
		Map<Group, Integer> filledGroups = getFilledGroups(); 
		Map<Group, Integer> nonFilledGroups = getGroupsToFill(filledGroups); 
		List<Group> vacantGroups = createVacantGroupList(nonFilledGroups);
		int i = 0;
		while(i<teamsToDistr.size() && vacantGroups.size()>0) {
			int placeInList = (int)(vacantGroups.size() * Math.random());
			teamsToDistr.get(i).setGroup(vacantGroups.remove(placeInList));
			i++;
		}
		teamsToDistr.forEach((Team team) ->{
			save(team);
		});
	}

	@Transactional
	private void save(Team team) {
		teamDao.save(team);
	}

	private List<Group> createVacantGroupList(Map<Group, Integer> nonFilledGroups) {
		List<Group> list = new ArrayList<Group>();
		if(nonFilledGroups != null) {
			nonFilledGroups.forEach((group, count) ->{
				list.addAll(Collections.nCopies(count, group));
			});
		}
		return list;
	}

	private static final int GROUP_CAPACITY = 4;
	private Map<Group, Integer> getGroupsToFill(Map<Group, Integer> filledGroups) {
		Set<Group> groups = groupDao.listGroups();
		Map<Group, Integer> map = new TreeMap<Group, Integer>();
		groups.forEach((Group group) -> {
			map.put(group, GROUP_CAPACITY - filledGroups.get(group));
		});
		return map;
	}

	private Map<Group, Integer> getFilledGroups() {
		Set<Group> groups = groupDao.listGroups();
		List<Team> teams = listTeams();
		Map<Group, Integer> map = initGroupMap(groups);
		if(teams != null) {
			teams.forEach((Team team) -> {
				if(team != null && team.getGroup() != null) {
					Group group = team.getGroup();
					int count = map.containsKey(group) ? map.get(group) : 0;
					map.put(group, count + 1);
				}
			});
		}
		return map;
	}

	private Map<Group, Integer> initGroupMap(Set<Group> groups) {
		Map<Group, Integer> map = new TreeMap<Group, Integer>();
		if(groups != null) {
			groups.forEach((Group group) -> map.put(group, 0));
		}
		return map;
	}

	@Transactional(readOnly = true)
	@Override
	public Team get(Long teamId) {
		return teamDao.get(teamId);
	}

}
