<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<div class="container">

		<div class="starter-template">
			<h1>Чемпионат по футболу</h1>
		</div>

		<div class="error-class">${error}</div>
	
		<h3>Группы</h3>
		<c:forEach var="group" items="${groups}">
			<div class="group-class">
				группа
				<div class="group-name-class">${group.name}</div>
				<c:forEach var="team" items="${group.teams}">
					${team.name}<br/>
				</c:forEach>
			</div>
		</c:forEach>
		
		<c:if test="${not empty undistributedTeams}">
			<div style="border: solid black 1px;">
				<form action="distributeTeams" method="post">
					<button >Распределить команды по группам</button>
				</form>
				<h2>Команды, нераспределённые в группы:</h2>
				<c:forEach var="entry" items="${undistributedTeams}">
					${entry} <br/>
				</c:forEach>
			</div>
		</c:if>

		<c:if test="${empty undistributedTeams}">
			<div style="border: solid darkblue 1px;">
				<form action="makeGame" method="post">
					<table border="0">
						<tr>
							<td>Команда 1:
								<select name="team1">
									<c:forEach var="team" items="${teams}">
									  <option value="${team.id}">${team.name} - ${team.group.name}</option>
									</c:forEach>
								</select>
							</td>
							<td>Команда 2:
								<select name="team2">
									<c:forEach var="team" items="${teams}">
									  <option value="${team.id}">${team.name} - ${team.group.name}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<td>Забито голов:
								<input type="text" name="goals1"></input>
							</td>
							<td>Забито голов:
								<input type="text" name="goals2"></input>
							</td>
						</tr>
					</table>
					<button >Зафиксировать результат матча</button>
				</form>
			</div>
		</c:if>

		<c:if test="${not empty games}">
			<h3>Проведённые матчи</h3>
			<c:forEach var="game" items="${games}">
				${game.groupName}: ${game.team1Name} - ${game.team2Name} - ${game.goals1} - ${game.goals2} <br/>   
			</c:forEach>
		</c:if>
		
		<c:if test="${not empty analyticBeans}">
			<h3>Аналитика</h3>
			<table class="analytic-class">
				<tr>
					<th>Команда</th>
					<th>Победы</th>
					<th>Ничьи</th>
					<th>Поражения</th>
					<th>Забито</th>
					<th>Пропущено</th>
					<th>Очки</th>
				</tr>
				<c:forEach var="analyticBean" items="${analyticBeans}">
					<tr>
						<td>${analyticBean.team.name}</td>
						<td>${analyticBean.wins}</td>
						<td>${analyticBean.draw}</td>
						<td>${analyticBean.defeat}</td>
						<td>${analyticBean.clogged}</td>
						<td>${analyticBean.missing}</td>
						<td>${analyticBean.scores}</td>
					</tr>
				</c:forEach>				
			</table>
		</c:if>
	</div>



<%--
	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
--%>
</body>

</html>
